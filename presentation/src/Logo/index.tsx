import "./style.scss";

import React from "react";
import logo from "../logo.png";
import classNames from "classnames";

interface Props {
  isShown: boolean;
  caption?: string;
}

const Logo = ({ isShown, caption }: Props) => {
  return (
    <div className="logo">
      <div className={classNames(`logo_plate`, { "-isShown": isShown })}>
        <img src={logo} alt="" className="logo_logo" />
      </div>
      {caption && (
        <div key={caption} className="logo_caption">
          {caption}
        </div>
      )}
    </div>
  );
};

export default Logo;
