import "./style.scss";

import React, { useEffect } from "react";

interface Props {
  path: string;
  fit: `contain` | `cover`;
  onNext(): void;
}

const Photo = ({ path, fit, onNext }: Props) => {
  useEffect(() => {
    const timeout = setTimeout(onNext, 5000);
    return () => clearTimeout(timeout);
  }, [onNext]);

  return <img className="photo" style={{ objectFit: fit }} src={path} alt="" />;
};

export default Photo;
