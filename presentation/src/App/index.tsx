import "./style.scss";
import React, { useEffect, useState, useMemo } from "react";
import { Project } from "types";
import TitleScreen from "../TitleScreen";
import VideoTrack from "../VideoTrack";
import Logo from "../Logo";

const { ipcRenderer } = __non_webpack_require__(
  `electron`,
) as typeof import("electron");

const App = () => {
  const [project, setProject] = useState<Project>({
    tracks: [],
    activeTrack: 0,
  });
  const [caption, setCaption] = useState<string | undefined>();

  useEffect(() => {
    ipcRenderer.on(`project`, (_, project) => setProject(project));
    ipcRenderer.send(`ready`);
  }, []);

  const activeTrack = useMemo(
    () => project.tracks.find(({ id }) => id === project.activeTrack),
    [project],
  );

  useEffect(() => {
    if (activeTrack?.type === `titleScreen`) {
      setCaption(undefined);
    }
  }, [activeTrack]);

  if (!activeTrack) {
    return null;
  }

  return (
    <>
      <Logo isShown={activeTrack.type === `video`} caption={caption} />
      {activeTrack.type === `titleScreen` && (
        <TitleScreen track={activeTrack} />
      )}
      {activeTrack.type === `video` && (
        <VideoTrack track={activeTrack} onCaption={setCaption} />
      )}
    </>
  );
};

export default App;
