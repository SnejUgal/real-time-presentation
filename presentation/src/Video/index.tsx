import "./style.scss";

import React from "react";

interface Props {
  path: string;
  fit: `contain` | `cover`;
  onNext(): void;
}

const Video = ({ path, fit, onNext }: Props) => {
  return (
    <video
      className="video"
      style={{ objectFit: fit }}
      src={path}
      onEnded={onNext}
      autoPlay={true}
    />
  );
};

export default Video;
