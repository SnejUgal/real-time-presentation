import React, { useState, useEffect, useCallback } from "react";

import { VideoTrack as IVideoTrack, CommonTrackData } from "types";
import Video from "../Video";
import Photo from "../Photo";

interface Props {
  track: IVideoTrack & CommonTrackData;
  onCaption(caption?: string): void;
}

const VideoTrack = ({ track, onCaption }: Props) => {
  const [key, setKey] = useState(0);

  const updateIds = useCallback(() => {
    const sectionId = track.sections[0]?.id;
    const compoentId = track.sections.find(({ id }) => id === sectionId)
      ?.components[0]?.id;

    return { section: sectionId, component: compoentId };
  }, [track]);

  const [ids, setIds] = useState(updateIds);

  useEffect(() => setIds(updateIds), [track, updateIds]);

  const next = useCallback(() => {
    const currentSectionIndex = track.sections.findIndex(
      ({ id }) => id === ids.section,
    );
    const currentSection = track.sections[currentSectionIndex];
    const currentComponentIndex = currentSection.components.findIndex(
      ({ id }) => id === ids.component,
    );

    const nextComponentIndex =
      (currentComponentIndex + 1) % currentSection.components.length;
    const nextSectionIndex =
      nextComponentIndex === 0
        ? (currentSectionIndex + 1) % track.sections.length
        : currentSectionIndex;

    const nextSection = track.sections[nextSectionIndex];
    const nextComponent = nextSection.components[nextComponentIndex];

    setIds({ section: nextSection.id, component: nextComponent.id });
    setKey(key => key + 1);
  }, [track, ids]);

  const section = track.sections.find(({ id }) => id === ids.section);

  useEffect(() => {
    if (section) {
      onCaption(section.title);
    }
  }, [section, onCaption]);

  if (!section) {
    return null;
  }

  const component = section.components.find(({ id }) => id === ids.component);

  if (!component) {
    return null;
  }

  if (component.type === `video`) {
    return (
      <Video
        key={key}
        path={component.path}
        fit={component.fit}
        onNext={next}
      />
    );
  }

  return (
    <Photo key={key} path={component.path} fit={component.fit} onNext={next} />
  );
};

export default VideoTrack;
