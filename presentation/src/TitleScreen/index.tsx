import "./style.scss";

import React from "react";
import { TitleScreen as ITitleScreen, CommonTrackData } from "types";
import logo from "../logo.png";

interface Props {
  track: ITitleScreen & CommonTrackData;
}

const TitleScreen = ({ track }: Props) => {
  return (
    <div className="titleScreen">
      <img className="titleScreen_logo" src={logo} alt="" />
      <h1 className="titleScreen_title">{track.name}</h1>
    </div>
  );
};

export default TitleScreen;
