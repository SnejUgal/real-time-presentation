import { app, BrowserWindow, screen, Notification } from "electron";
import { join } from "path";
import { Project } from "types";

let project: Project = {
  tracks: [
    {
      name: `Основной видеоряд`,
      id: 0,
      type: `video`,
      sections: [],
    },
  ],
  activeTrack: 0,
};

const initialize = () => {
  const primaryScreen = screen.getPrimaryDisplay();
  const [secondaryScreen] = screen
    .getAllDisplays()
    .filter(({ id }) => id !== primaryScreen.id);

  if (!secondaryScreen) {
    new Notification({
      title: `Подключите проектор как второй экран`,
      body: `Презентация в реальном времени работает только при проекторе, настроенном как второй экран`,
    }).show();
    process.exit();
  }

  const editor = new BrowserWindow({
    width: primaryScreen.workArea.width * (2 / 3),
    height: primaryScreen.workArea.height * (2 / 3),
    x: primaryScreen.workArea.x + primaryScreen.workArea.width / 6,
    y: primaryScreen.workArea.y + primaryScreen.workArea.height / 6,
    autoHideMenuBar: true,
    webPreferences: { nodeIntegration: true },
  });

  editor.webContents.on(`ipc-message`, (_, channel, newProject) => {
    if (channel === `ready`) {
      editor.webContents.send(`project`, project);
    } else if (channel === `project`) {
      project = newProject;
      presentation.webContents.send(`project`, project);
    }
  });

  editor.loadFile(join(__dirname, `../editor/build/index.html`));

  const presentation = new BrowserWindow({
    x: secondaryScreen.workArea.x,
    y: secondaryScreen.workArea.y,
    fullscreen: true,
    backgroundColor: `black`,
    autoHideMenuBar: true,
    webPreferences: { nodeIntegration: true },
  });

  presentation.loadFile(join(__dirname, `../presentation/build/index.html`));

  presentation.webContents.on(`ipc-message`, (_, channel) => {
    if (channel === `ready`) {
      presentation.webContents.send(`project`, project);
    }
  });
};

app.whenReady().then(initialize);
