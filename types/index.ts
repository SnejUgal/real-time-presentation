export interface Project {
  tracks: Track[];
  activeTrack: number;
}

export type Track = (VideoTrack | TitleScreen) & CommonTrackData;

export interface CommonTrackData {
  id: number;
  name: string;
}

export interface TitleScreen {
  type: `titleScreen`;
}

export interface VideoTrack {
  type: `video`;
  sections: Section[];
}

export interface Section {
  id: number;
  title: string;
  components: Component[];
}

export type Component = (Photo | Video) & CommonComponentData;

export interface CommonComponentData {
  id: number;
}

export interface Photo {
  type: `photo`;
  path: string;
  fit: `cover` | `contain`;
}

export interface Video {
  type: `video`;
  path: string;
  fit: `cover` | `contain`;
}
