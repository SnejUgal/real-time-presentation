import "./style.scss";

import React, { useEffect, useState } from "react";
import classNames from "classnames";

interface Props {
  id: string;
  label: string;
  value: string;
  className?: string;
  onChange(newValue: string): void;
}

const TextField = ({ id, label, value, onChange, className }: Props) => {
  const [visibleValue, setVisibleValue] = useState(value);
  const [isFocused, setIsFocused] = useState(false);

  useEffect(() => setVisibleValue(value), [value]);

  const textFieldClassName = classNames(`textField`, className, {
    "-isFocused": isFocused,
  });
  const labelClassName = classNames(`textField_label`, {
    "-isExpanded": visibleValue.length === 0 && !isFocused,
    "-isFocused": isFocused,
  });

  return (
    <div className={textFieldClassName}>
      <label className={labelClassName} htmlFor={id}>
        {label}
      </label>
      <input
        className="textField_input"
        id={id}
        value={visibleValue}
        onChange={event => setVisibleValue(event.target.value)}
        type="text"
        onFocus={() => {
          setIsFocused(true);
        }}
        onBlur={() => {
          setIsFocused(false);
          onChange(visibleValue);
        }}
        onKeyDown={event => {
          if (event.key === `Enter`) {
            event.currentTarget.blur();
          }
        }}
      />
    </div>
  );
};

export default TextField;
