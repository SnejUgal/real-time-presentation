import "./style.scss";

import React, { useState, useEffect } from "react";
import { Project, VideoTrack } from "types";
import Button from "../Button";
import Track from "../Track";
import { DragDropContext } from "react-beautiful-dnd";

const { ipcRenderer } = __non_webpack_require__(
  `electron`,
) as typeof import("electron");

function App() {
  const [project, setProject] = useState<Project>({
    tracks: [],
    activeTrack: 0,
  });

  console.log(project);

  const getNewTrackId = () => {
    const id = Math.max(...project.tracks.map(track => track.id)) + 1;

    if (Number.isFinite(id)) {
      return id;
    }

    return 0;
  };

  const getNewSectionId = () => {
    const id =
      Math.max(
        ...project.tracks
          .filter(track => track.type === `video`)
          .flatMap(track =>
            (track as VideoTrack).sections.map(section => section.id),
          ),
      ) + 1;

    if (Number.isFinite(id)) {
      return id;
    }

    return 0;
  };

  const getNewComponentId = () => {
    const id =
      Math.max(
        ...project.tracks
          .filter(track => track.type === `video`)
          .flatMap(track =>
            (track as VideoTrack).sections.flatMap(section =>
              section.components.map(section => section.id),
            ),
          ),
      ) + 1;

    if (Number.isFinite(id)) {
      return id;
    }

    return 0;
  };

  useEffect(() => {
    ipcRenderer.on(`project`, (_, project) => setProject(project));
    ipcRenderer.send(`ready`);
  }, []);

  useEffect(() => {
    ipcRenderer.send(`project`, project);
  }, [project]);

  return (
    <DragDropContext
      onDragEnd={result => {
        if (result.destination === undefined) {
          return;
        }

        const sourceSectionId = parseInt(
          result.source.droppableId.split(`-`)[1],
        );
        const destinationSectionId = parseInt(
          result.destination.droppableId.split(`-`)[1],
        );

        let tracks = project.tracks;

        let sourceSection = tracks
          .filter(track => track.type === `video`)
          .flatMap(track => (track as VideoTrack).sections)
          .find(section => section.id === sourceSectionId);
        let destinationSection = tracks
          .filter(track => track.type === `video`)
          .flatMap(track => (track as VideoTrack).sections)
          .find(section => section.id === destinationSectionId);

        if (sourceSection === undefined || destinationSection === undefined) {
          return;
        }

        let component = sourceSection.components.splice(
          result.source.index,
          1,
        )[0];
        destinationSection.components.splice(
          result.destination.index,
          0,
          component,
        );

        setProject({ ...project, tracks });
      }}
    >
      {project.tracks.map((track, index) => (
        <Track
          key={track.id}
          track={track}
          isActive={track.id === project.activeTrack}
          onChange={track => {
            const tracks = [...project.tracks];
            tracks[index] = track;
            setProject({ ...project, tracks });
          }}
          onActivate={() => setProject({ ...project, activeTrack: track.id })}
          onDelete={() =>
            setProject({
              ...project,
              tracks: project.tracks.filter(({ id }) => id !== track.id),
            })
          }
          getNewSectionId={getNewSectionId}
          getNewComponentId={getNewComponentId}
        />
      ))}
      <div className="create">
        <Button
          onClick={() =>
            setProject({
              ...project,
              tracks: [
                ...project.tracks,
                {
                  type: `video`,
                  id: getNewTrackId(),
                  name: `Видеоряд`,
                  sections: [],
                },
              ],
            })
          }
        >
          Создать видеоряд
        </Button>
        <Button
          onClick={() =>
            setProject({
              ...project,
              tracks: [
                ...project.tracks,
                {
                  type: `titleScreen`,
                  id: getNewTrackId(),
                  name: `Заголовок`,
                },
              ],
            })
          }
        >
          Создать заголовок
        </Button>
      </div>
    </DragDropContext>
  );
}

export default App;
