import "./style.scss";

import React from "react";
import { Component as IComponent } from "types";
import Button from "../Button";
import fullscreen from "./icons/fullscreen.svg";
import fullscreenExit from "./icons/fullscreen-exit.svg";
import { Draggable } from "react-beautiful-dnd";

interface Props {
  component: IComponent;
  componentIndex: number;
  onDelete(): void;
  onToggle(): void;
}

const Component = ({ component, componentIndex, onDelete, onToggle }: Props) => {
  return (
    <Draggable draggableId={`draggable-${component.id}`} index={componentIndex}>
      {(provided, snapshot) => (
        <li
          ref={provided.innerRef}
          {...provided.draggableProps}
          {...provided.dragHandleProps}
          className="component"
        >
          {component.type === `photo` && (
            <img className="component_photo" src={component.path} alt="" />
          )}
          {component.type === `video` && (
            <video className="component_video" src={component.path} />
          )}
          <div className="component_overlay">
            <Button className="component_delete" onClick={onDelete}>
              Удалить
            </Button>
            <Button className="component_fitToggle" onClick={onToggle}>
              <img
                src={component.fit === `contain` ? fullscreen : fullscreenExit}
              />
            </Button>
          </div>
        </li>
      )}
    </Draggable>
  );
};

export default Component;
