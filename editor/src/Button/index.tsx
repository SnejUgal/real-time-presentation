import "./style.scss";

import React from "react";
import classNames from "classnames";

interface FilesButton {
  type: `files`;
  id: string;
  onFiles(files: File[]): void;
  isMultiple?: boolean;
  accepts?: string;
}

interface RegularButton {
  onClick(): void;
}

interface CommonProps {
  children: React.ReactNode;
  isDisabled?: boolean;
  className?: string;
}

type Props = (RegularButton | FilesButton) & CommonProps;

const Button = (props: Props) => {
  const className = classNames(`button`, props.className);
  if (`type` in props) {
    return (
      <div className="fileButton">
        <input
          type="file"
          id={props.id}
          className="fileButton_input"
          onChange={event => props.onFiles([...event.currentTarget.files])}
          disabled={props.isDisabled}
          multiple={props.isMultiple}
          accept={props.accepts}
        />
        <label htmlFor={props.id} className={className}>
          {props.children}
        </label>
      </div>
    );
  }

  return (
    <button
      type="button"
      onClick={props.onClick}
      disabled={props.isDisabled}
      className={className}
    >
      {props.children}
    </button>
  );
};

export default Button;
