import "./style.scss";

import React from "react";
import { Section as ISection } from "types";
import TextField from "../TextField";
import Button from "../Button";
import Component from "../Component";
import { extname } from "path";
import { Droppable } from "react-beautiful-dnd";

const PHOTOS = [`.jpg`, `.png`];
const VIDEOS = [`.mp4`, `.mkv`];

interface Props {
  trackId: number;
  section: ISection;
  onChange(section: ISection): void;
  onDelete(): void;
  getNewComponentId(): number;
}

const Section = ({
  trackId,
  section,
  onChange,
  onDelete,
  getNewComponentId,
}: Props) => {
  return (
    <li className="section">
      <TextField
        id={`section-${section.id}`}
        className="section_name"
        label="Название раздела"
        value={section.title}
        onChange={title => onChange({ ...section, title })}
      />
      <Button className="section_delete" onClick={onDelete}>
        Удалить
      </Button>
      <Droppable direction="horizontal" droppableId={`droppable-${section.id}`}>
        {(provided, snapshot) => (
          <ul
            className="section_components"
            ref={provided.innerRef}
            {...provided.droppableProps}
          >
            {section.components.map((component, index) => (
              <Component
                key={component.id}
                component={component}
                componentIndex={index}
                onDelete={() =>
                  onChange({
                    ...section,
                    components: section.components.filter(
                      ({ id }) => id !== component.id,
                    ),
                  })
                }
                onToggle={() =>
                  onChange({
                    ...section,
                    components: section.components.map(x =>
                      x.id === component.id
                        ? {
                            ...component,
                            fit:
                              component.fit === `contain` ? `cover` : `contain`,
                          }
                        : x,
                    ),
                  })
                }
              />
            ))}
            {provided.placeholder}
            <Button
              type="files"
              id={`file-${section.id}`}
              className="track_create"
              onFiles={files => {
                onChange({
                  ...section,
                  components: [
                    ...section.components,
                    ...files.map((file, index) => {
                      if (PHOTOS.includes(extname(file.path))) {
                        return {
                          id: getNewComponentId() + index,
                          type: `photo` as const,
                          path: `file://${file.path}`,
                          fit: `contain` as const,
                        };
                      }

                      return {
                        id: getNewComponentId() + index,
                        type: `video` as const,
                        path: `file://${file.path}`,
                        fit: `contain` as const,
                      };
                    }),
                  ],
                });
              }}
              isMultiple={true}
              accepts="image/*,video/*"
            >
              Загрузить фото и видео
            </Button>
          </ul>
        )}
      </Droppable>
    </li>
  );
};

export default Section;
