import "./style.scss";

import React from "react";
import { Track as ITrack } from "types";
import Button from "../Button";
import TextField from "../TextField";
import Section from "../Section";

interface Props {
  track: ITrack;
  isActive: boolean;
  onChange(track: ITrack): void;
  onActivate(): void;
  onDelete(): void;
  getNewSectionId(): number;
  getNewComponentId(): number;
}

const Track = ({ track, isActive, onActivate, onChange, onDelete, getNewSectionId, getNewComponentId }: Props) => {
  return (
    <div className={`track -${track.type}`}>
      <div className="track_name">
        <TextField
          className="track_nameInput"
          label={track.type === `video` ? `Название видеоряда` : `Заголовок`}
          onChange={name => onChange({ ...track, name })}
          id={`track_${track.id}`}
          value={track.name}
        />
      </div>
      <Button className="track_delete" isDisabled={isActive} onClick={onDelete}>
        Удалить
      </Button>
      <Button
        className="track_activate"
        isDisabled={isActive}
        onClick={onActivate}
      >
        {isActive ? `Активен` : `Активировать`}
      </Button>
      {track.type === `video` && (
        <ul className="track_sections">
          {track.sections.map((section, index) => (
            <Section
              key={section.id}
              trackId={track.id}
              section={section}
              onChange={section => {
                const sections = [...track.sections];
                sections[index] = section;
                onChange({ ...track, sections });
              }}
              onDelete={() =>
                onChange({
                  ...track,
                  sections: track.sections.filter(
                    ({ id }) => id !== section.id,
                  ),
                })
              }
              getNewComponentId={getNewComponentId}
            />
          ))}
          <Button
            className="track_create"
            onClick={() => {
              let id = getNewSectionId();

              onChange({
                ...track,
                sections: [
                  ...track.sections,
                  {
                    id,
                    title: `Раздел`,
                    components: [],
                  },
                ],
              });
            }}
          >
            Создать раздел
          </Button>
        </ul>
      )}
    </div>
  );
};

export default Track;
