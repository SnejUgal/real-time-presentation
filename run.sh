#!/bin/bash

set -e
npm i
npm run build
cd editor
npm i
npm run build
cd ../presentation
npm i
npm run build
cd ..
npm start
